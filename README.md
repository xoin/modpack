# Оновлення модпаку. 1.20.1

Статуси:

- ✅ - Новий
- 🔄 - Перенесено зі старого паку
- ❌ - Видалено

| Ім'я                                    | Статус |
| --------------------------------------- | ------ |
| Aether Addon: Enhanced Extinguishing    | ✅     |
| Aether: Lost Content Addon              | ✅     |
| Alex's Mobs                             | 🔄     |
| Alternate Current                       | ✅     |
| Another Furniture                       | ✅     |
| Architectury API (Fabric/Forge)         | ✅     |
| Athena                                  | ✅     |
| AttributeFix                            | ✅     |
| Backpacked                              | ✅     |
| Bad Mobs                                | ✅     |
| Better Compatibility Checker            | ✅     |
| Biomes O' Plenty                        | 🔄     |
| Blossom Blade                           | ✅     |
| Blue Skies                              | 🔄     |
| Boat Break Fix [Neo/Forge/Fabric/Quilt] | ✅     |
| Bountiful (Forge)                       | 🔄     |
| Carry On                                | ✅     |
| Catalogue                               | ✅     |
| Charm of Undying (Fabric/Forge/Quilt)   | ✅     |
| Chat Heads                              | ✅     |
| Chef's Delight [Forge]                  | ✅     |
| Chunk Sending[Forge/Fabric]             | ✅     |
| Citadel                                 | 🔄     |
| Cloth Config API (Fabric/Forge)         | ✅     |
| Collective                              | 🔄     |
| Connectivity[Forge/Fabric]              | ✅     |
| Controllable (Forge)                    | ✅     |
| Corpse                                  | 🔄     |
| Cuisine Delight                         | ✅     |
| Cull Leaves                             | ✅     |
| Cupboard                                | ✅     |
| Curios API (Forge)                      | ✅     |
| Decorative Blocks (Fork)                | ✅     |
| Deeper and Darker                       | ✅     |
| Double Doors                            | 🔄     |
| Dragon Mounts: Legacy                   | 🔄     |
| Easy Anvils [Forge & Fabric]            | ✅     |
| Easy Magic [Forge & Fabric]             | ✅     |
| End's Delight                           | ✅     |
| FallingTree (Forge&Fabric)              | 🔄     |
| Farmer's Delight                        | ✅     |
| FerriteCore (Forge)                     | 🔄     |
| Framework                               | ✅     |
| FTB Essentials (Forge & Fabric)         | ✅     |
| FTB Library (Forge)                     | ✅     |
| FTB Ranks (Forge)                       | ✅     |
| Handcrafted                             | ✅     |
| Hearth & Home                           | ✅     |
| Illager Invasion [Forge & Fabric]       | ✅     |
| ImmediatelyFast                         | ✅     |
| Jade 🔍                                 | 🔄     |
| Just Enough Items (JEI)                 | 🔄     |
| Just Enough Resources (JER)             | 🔄     |
| Kambrik                                 | ✅     |
| Kotlin for Forge                        | 🔄     |
| Lucent                                  | ✅     |
| Macaw's Bridges                         | 🔄     |
| Macaw's Bridges - Biomes O' Plenty      | 🔄     |
| Macaw's Fences - Biomes O' Plenty       | 🔄     |
| Macaw's Fences and Walls                | 🔄     |
| Macaw's Roofs                           | 🔄     |
| Macaw's Roofs - Biomes O' Plenty        | 🔄     |
| ModernFix                               | 🔄     |
| Music Manager                           | ✅     |
| Necronomicon API                        | ✅     |
| Nether's Delight                        | ✅     |
| No Chat Reports                         | ✅     |
| Ocean's Delight                         | ✅     |
| Puzzles Lib [Forge & Fabric]            | ✅     |
| Repurposed Structures (Neoforge/Forge)  | ✅     |
| Resourceful Lib                         | ✅     |
| Security Craft                          | 🔄     |
| Structure Gel API                       | 🔄     |
| TerraBlender (Forge)                    | ✅     |
| The Aether                              | 🔄     |
| The One Probe                           | 🔄     |
| The Twilight Forest                     | 🔄     |
| Tidal Towns                             | ✅     |
| Twilight's Flavors & Delight            | ✅     |
| WTHIT Forge Edition                     | ✅     |
| YUNG's API (Forge)                      | 🔄     |
| YUNG's Better Dungeons (Forge)          | 🔄     |
| YUNG's Better End Island (Forge)        | 🔄     |
| YUNG's Better Jungle Temples (Forge)    | 🔄     |
| YUNG's Better Mineshafts (Forge)        | 🔄     |
| YUNG's Better Nether Fortresses (Forge) | 🔄     |
| YUNG's Better Ocean Monuments (Forge)   | 🔄     |
| YUNG's Better Witch Huts (Forge)        | 🔄     |
| YUNG's Bridges (Forge)                  | 🔄     |
| YUNG's Extras (Forge)                   | 🔄     |
| YUNG's Menu Tweaks (Forge)              | 🔄     |
| Draconic Evolution                      | ❌     |
| Mekanism                                | ❌     |
| Modular Powersuits                      | ❌     |
| Numina                                  | ❌     |
| Powah                                   | ❌     |
| Thermal Expansion                       | ❌     |
| Thermal Foundation                      | ❌     |
